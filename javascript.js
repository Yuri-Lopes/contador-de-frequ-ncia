document.getElementById("countButton").onclick = function() {  

    let typedText = document.getElementById("textInput").value;
    
    const letterCounts = {};
    const wordsCounts = {};
    
    typedText = typedText.toLowerCase(); 
    // Isto muda todas as letras para minúsculas
    
    typedText = typedText.replace(/[^a-z'\s]+/g);
    // Isso se livra de todos os caracteres exceto letras comuns, espaços e apóstrofos. 
    // Iremos aprender mais sobre como usar a função replace numa lição mais à frente.
        
        for (let i = 0; i < typedText.length; i++) {
            currentLetter = typedText[i];
    
            if (letterCounts[currentLetter] === undefined) {
                letterCounts[currentLetter] = 1; 
            } else { 
                letterCounts[currentLetter]++; 
            }
        }
    
    for (let letter in letterCounts) { 
        const span = document.createElement("span"); 
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", "); 
        span.appendChild(textContent); 
        document.getElementById("lettersDiv").appendChild(span); 
    }
    words = typedText.split(/\s/);
    
        for (let i = 0; i < words.length; i++) {
            currentwords = words[i];
    
            if (wordsCounts[currentwords] === undefined) {
                wordsCounts[currentwords] = 1;
            } else {
                wordsCounts[currentwords]++;
            }
    
        }
    
        for (let words in wordsCounts) {
            const span = document.createElement("span");
            const textContent = document.createTextNode('"' + words + "\": " + wordsCounts[words] + ", ");
            span.appendChild(textContent);
            document.getElementById("wordsDiv").appendChild(span); 
        }
    }
    